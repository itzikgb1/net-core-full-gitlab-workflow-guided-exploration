## Setup This Design Pattern in a New Repository

These instructions assume [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) for change managing environment promotion using GitOps methodologies.

### 1. Copy This Repository to Your Own Location

1. * [ ] In the original repository, on the left navigation bar, click **Project overview => Details**
2. * [ ] From the upper right of the page click **Clone** (Dropdown button) 
3. * [ ] From the _Clone with HTTPS_ field, click the Copy Icon to copy the https:// repo url.
4. * [ ] While in the desired target group in **any GitLab instance**, click the **"+"** icon at the top of the screen and select "New Project"
5. * [ ] From the "New Project" page, click **Import project** (tab)
6. * [ ] Click **Repo by URL**

> Note: if this option is missing it may be disabled in your instance, consult your GitLab instance administrator.

7. * [ ] In **Git repository URL** paste the copied URL. (Note: it must be https://)
8. * [ ] Set the following fields to your preferences: **Project name**, **Project URL**, **Project slug**, **Visibility Level**
9. * [ ] Click **Create project**
10. * [ ] In the new repository you made from this copy, restart this checklist as a GitLab issue so that: you can track your a) individual progress with b) an interactive checklist where you can checkoff each step, by:

### 2. Customize the Repository Settings

In the fresh repository copy:

1. * [ ] From the _left navigation_



## 3 Create Personal Exploration Checklists
Guided explorations are implemented as GitLab issue templates so they can be used to create a personal progress tracking checklist for anyone wishing to walk through the exploration.

When in an issue, the checklist steps become clickable (without changing to markdown edit mode) and can be used to track personal progress by many individuals.

1. * [ ] On the left navigation of this repository click **Issues**
2. * [ ] Click **New issue**
3. * [ ] On the _New Issue_ page, next to _Title_, click **Choose a template** and select one of the templates containing the text **Guided-Exploration**
4. * [ ] The new issue created allows you to check items off as you complete them.