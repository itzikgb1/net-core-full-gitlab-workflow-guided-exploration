## Working Design Pattern

As originally built, this design pattern works and can be tested by changing files and seeing builds run.
> **Note:** You can examine closed and in-progress merge requests and issues as well as past pipeline status and logs to preview the flow and controls before attempting your own implementation.

## Guided Exploration Exercises Available
Guided Explorations are designed to be used for training, feature exploration or demonstrations.

This repository contains one or more issue templates designed to guide you through:

* setting it up in another location
* observing the major implementation features
* noting what customizations might be made
* make small changes to observe builds

>  **Note:** To enable Guided Explorations: Setup a duplicate of this repository using these instructions: [.gitlab/issue_templates/01-Setup-Repository.md](.gitlab/issue_templates/01-Setup-Repository.md)  The instructions will eventually have you create a new issue from a template to use as your own Guided Exploration with checklist support for what you have completed.  Each member of a team can use the issue template to track the progress and complete the exploration.

## Demonstrates These Design Requirements

- 
